function init(firstTime = true) {
  let startGame = true;
  if (firstTime) {
    alert(
      "¡Bienvenido al juego de adivinanzas! Tienes 3 intentos para adivinar un número entre 1 y 10."
    );
  } else {
    let initGame = prompt(`Deseas seguir jugando? Escribe Si o No`);
    if (!initGame || initGame.toLowerCase() != "si") startGame = false;
  }

  if (startGame) game();
}

function game() {
  const randomNumber = Math.floor(Math.random() * 10) + 1; // Numero random del 1 al 10.
  let attempts = 1; // Cantidad de intentos
  let message; // Donde guardamos el texto que queremos mostrarle al usuario

  // Ciclo para permitir al usuario adivinar múltiples veces, que se va a repetir mientras se cumpla la condicion
  do {
    let answer = prompt(`Introduce tu ${attempts}° respuesta:`);
    if (parseInt(answer) === randomNumber) {
      message = "¡Felicidades! ¡Has adivinado el número!";
      alert(message);
      init(false);
      break;
    } else if (attempts == 3) {
      message = "Respuesta incorrecta. Has fracasado :( GAME OVER ";
      alert(message);
      init(false);
      break;
    } else {
      if (!parseInt(answer)) {
        message =
          "Respuesta incorrecta. El juego unicamente se trata de numeros...";
      } else {
        message = "Respuesta incorrecta. Intenta de nuevo.";
      }
      alert(message);
      attempts++;
    }
  } while (attempts < 4);
}

init();
